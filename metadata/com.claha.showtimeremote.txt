Categories:Multimedia
License:GPLv3
Web Site:https://showtimemediacenter.com
Source Code:https://github.com/claha/showtimeremote
Issue Tracker:https://github.com/claha/showtimeremote/issues

Auto Name:Showtime Remote
Summary:Control a Showtime media center
Description:
Remote control for a [https://showtimemediacenter.com/ Showtime] media center
instance. Features:

* Buttons for most common actions, see below for a more detailed description
* Use the integrated searchbar to search in Showtime
* Use profiles if you are running Showtime on multiple devices
* Swipe to switch between profiles
* Swipe to switch between navigation and media remote

[https://github.com/claha/showtimeremote/blob/HEAD/CHANGELOG.md Changelog]
.

Repo Type:git
Repo:https://github.com/claha/showtimeremote

Build:1.0,1
    commit=v1.0.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1

